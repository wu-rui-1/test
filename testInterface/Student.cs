﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace testInterface
{
    public class Student : Person
    {

        public Student(string name, int age, char gender, string hobby)
            : base(name, age, gender)
        {
            this.Hobby = hobby;
        }
        private string _hobby;
        public string Hobby
        {
            get { return _hobby; }
            set { _hobby = value; }
        }
        public override string GetPersonInfo()
        {
            return "我是" +this.Name+"今年"+this.Age+",性别"+this.Gender+ "，我的爱好"+this.Hobby;
        }
    }

}
