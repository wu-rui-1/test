﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace testInterface
{
    class Teacher:Person
    {

        public Teacher(string name, int age, char gender, string subject)
               : base(name, age, gender)
        {
            this.Subject = subject;
        }
        private string _subject;
        public string Subject
        {
            get { return _subject; }
            set { _subject = value; }
        }
        public override string GetPersonInfo()
        {
            return "我是" + this.Name + "今年" + this.Age + ",性别" + this.Gender + "，我教的科目" + this.Subject;
        }
    }
}
