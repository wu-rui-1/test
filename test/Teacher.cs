﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace test
{
    class Teacher:Person
    {

        public Teacher(string name, int age, char gender, string subject)
               : base(name, age, gender)
        {
            this.Subject = subject;
        }
        private string _subject;
        public string Subject
        {
            get { return _subject; }
            set { _subject = value; }
        }
        public override string GetPersonInfo()
        {
            return base.GetPersonInfo() + "，我教科目" + this.Subject;
        }
    }
}
