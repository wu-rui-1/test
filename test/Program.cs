﻿using System;

namespace test
{
    class Program
    {
        static void Main(string[] args)
        {
            Person p1 = new Person("张三", 23, '男');
            Console.WriteLine("个人情况："+p1.GetPersonInfo());

            Person p2 = new Student("李四", 23, '男',"游泳");
            Console.WriteLine("个人情况：" + p2.GetPersonInfo());

            Person p3 = new Teacher("王二", 23, '男', "物理");
            Console.WriteLine("个人情况：" + p3.GetPersonInfo());
        }
    }
}
    

