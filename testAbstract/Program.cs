﻿using System;

namespace testAbstract
{
    class Program
    {
        static void Main(string[] args)
        {
            //   Person p1=new Person("Tom",15,‘男'); 无法创建抽象类对象,如果能创建对象，
            //   就可以调用Person类对象的方法，而Person对象方法没有实现体，
            Person p1 = new Student("Tom",23,'M', "Basketball");//此处可以 Student p1=new Student("Tom",23,'M', "Basketball");
            Person p2=new Teacher("Hilti", 46, 'F', "Computer");
            Output(p1);//为什么会调用Student的GetPersonInfo
            Output(p2);//为什么会调Teacher的GetPersonInfo
        }


        public static void Output(Person p)//此处用基类可以适用Student,Teacher,如将来增加派生类也可以用。
        {
            Console.WriteLine(p.GetPersonInfo());
        }
    }
}
